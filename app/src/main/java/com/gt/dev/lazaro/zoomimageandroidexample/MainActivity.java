package com.gt.dev.lazaro.zoomimageandroidexample;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.ChangeImageTransform;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private ImageView ivMain;
    private ViewGroup transitionContainer;
    boolean mExpanded;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // We initialize the widgets our widgets
        startWidgets();
    }

    private void startWidgets() {
        // To make a success animation we have to call our parent layout with the ViewGroup class
        transitionContainer = (ViewGroup) findViewById(R.id.cl_main);
        ivMain = (ImageView) findViewById(R.id.iv_main);

        ivMain.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.iv_main:
                mExpanded = !mExpanded;

                // We are going to use the library from andkulikov
                // Remember to import clases from TransitionsEverywhere library
                TransitionManager.beginDelayedTransition(transitionContainer, new TransitionSet()
                        .addTransition(new ChangeBounds())
                        .addTransition(new ChangeImageTransform()));

                ViewGroup.LayoutParams params = ivMain.getLayoutParams();
                // Finally we pass the params that ImageView needs
                // Make the zoom animation on our image
                params.height = mExpanded ? ViewGroup.LayoutParams.MATCH_PARENT : ViewGroup.LayoutParams.WRAP_CONTENT;
                // Set the params
                ivMain.setLayoutParams(params);
                // How it looks our ImageView when the user press the Picture
                ivMain.setScaleType(mExpanded ? ImageView.ScaleType.CENTER_CROP : ImageView.ScaleType.FIT_CENTER);
                break;
        }
    }
}
